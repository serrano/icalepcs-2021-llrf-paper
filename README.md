# ICALEPCS 2021 LLRF paper

LaTeX sources for ICALEPCS 2021 paper on WR and LLRF at CERN's SPS. See
https://www.jacow.org/Authors/LaTeX for information about the LaTeX template.
