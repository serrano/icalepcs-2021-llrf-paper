all : THBR02.pdf

.PHONY : all clean

THBR02.pdf : THBR02.tex 
	pdflatex $^
	pdflatex $^

clean :
	rm -f THBR02.pdf *.dat *.log *.out *.aux *.dvi *.ps *.nav *.snm *.toc *.vrb *~
